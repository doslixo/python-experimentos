
import requests as rq
import rdm_url

from bs4 import BeautifulSoup as bs


def gera_camps():
    url = rdm_url.gerador_nome(4)
    req = rq.get(url, timeout= 4)
    
    cp ={'url': url,
         'status_code': req.status_code,
         'server': req.headers['Server'] }
    
    return cp

print(gera_camps())
